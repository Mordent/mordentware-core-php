<?php
	namespace Mordentware\Core;

	/**
	 * Generic base class with core attribute methods.
	 *
	 * @method  void   clear
	 * @method  mixed  get
	 * @method  bool   has
	 * @method  void   set
	 */
	class Base {

    protected $attributes;

		/**
		 * @param  array  [attributes]  Array of key/value pairs of attributes.
		 */
    public function __construct($attributes = array()) {
      $this->attributes = $attributes;
    }

    /**
     * @return  string  HTML representation of generic object. Should not be displayed to user, but useful for debugging.
     */
    public function __toString() {
      $result = '(' . get_class($this) . '):<ul>';
      $result .= '<li>attributes (array):<ul>';
      foreach ($this->attributes as $attribute => $value) {
        $result .= '<li>' . $attribute . ': ' . $value . '</li>';
      }
      $result .= '</ul></li>';
      $result .= '</ul>';
      return $result;
    }

    /**
     * @param   string  attribute  Attribute to unset.
     * @return  void
     */
    public function clear($attribute) {
      unset($this->attributes[$attribute]);
    }

    /**
     * @param   string  [attribute]  Attribute to get value of, or null to retrieve all attributes.
     * @return  mixed                Value of attribute.
     */
    public function get($attribute = null) {
      $value;
      if (is_null($attribute)) {
        $value = $this->attributes;
      }
      else {
        if (!$this->has($attribute)) {
          throw new \OutOfBoundsException('Attribute "' . $attribute . '" has not been set.');
        }
        $value = $this->attributes[$attribute];
      }
      return $value;
    }

    /**
     * @param   string  attribute  Attribute to check.
     * @return  bool               Whether attribute is currently set.
     */
    public function has($attribute) {
      return array_key_exists($attribute, $this->attributes);
    }

    /**
     * @param   mixed  attribute  Attribute to set, or array of key/value pairs of attributes to set.
     * @param   mixed  [value]    Value to set attribute to if setting single attribute.
     * @return  void
     */
    public function set($attribute, $value = null) {
      if (is_array($attribute)) {
        $this->attributes = array_merge($this->attributes, $attribute);
      }
      else if (is_string($attribute)) {
        $this->attributes[$attribute] = $value;
      }
      else {
        throw new \InvalidArgumentException('Unable to set attribute.');
      }
    }

	}
?>